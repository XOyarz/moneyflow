# # syntax=docker/dockerfile:1
# FROM python:3
# ENV PYTHONDONTWRITEBYTECODE=1
# ENV PYTHONUNBUFFERED=1
# WORKDIR /moneyflow
# COPY requirements.txt /moneyflow/
# RUN pip install -r requirements.txt
# COPY . /moneyflow/

# EXPOSE 8000
# STOPSIGNAL SIGTERM
# CMD "python manage.py runserver"

FROM python:3.9
WORKDIR /moneyflow
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1  

COPY requirements.txt requirements.txt
RUN apt-get update && \
    apt-get install -y curl && \
    pip install --upgrade pip && \
    pip install -r requirements.txt


COPY ./moneyflow /moneyflow/
EXPOSE 8000
CMD python manage.py runserver