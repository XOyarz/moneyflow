from django import forms
from . import models

class ArticleForm(forms.Form):
    class Meta:
        model = models.Article
        fields = ('title', 'text')

class ArticleCreateForm(forms.ModelForm):
    class Meta:
        model = models.Article
        fields = ('title', 'text', 'related_articles')
        widgets = {
            'text': forms.Textarea(),
            'related_articles': forms.CheckboxSelectMultiple()
        }


class ArticleEditForm(forms.ModelForm):
    class Meta:
        model = models.Article
        fields = ('title', 'text', 'related_articles')
        widgets = {
            'text': forms.Textarea(),
            'related_articles': forms.CheckboxSelectMultiple()
        }

    def validate_related_articles(self, article_id):
        if str(article_id) in self['related_articles'].value():
            raise forms.ValidationError(message="This article cannot link to itself")