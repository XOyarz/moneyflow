from django.db import models

# Create your models here.

class Article(models.Model):
    title = models.CharField(max_length=100)
    text = models.TextField()
    related_articles = models.ManyToManyField("self", symmetrical=False, blank=True)
    created = models.DateTimeField(
        auto_now_add=True
    )
    modified = models.DateTimeField(
        auto_now=True
    )
    view_count = models.IntegerField(default=0)

