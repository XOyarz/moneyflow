import datetime

from django.test import TestCase
from .models import Article
from django.urls import reverse

def create_article(title, text):
    return Article.objects.create(title=title, text=text)

class ArticleTestCase(TestCase):

    def test_created_date_equals_modified_date_on_creation(self):
        """
        was_published_recently() returns False for questions whose pub_date
        is in the future.
        """
        article = Article(title="New Title", text="New Text")
        self.assertEquals(article.created, article.modified)

class IndexViewTests(TestCase):
    def test_index_returns_200(self):
        response = self.client.get(reverse('index'))
        self.assertEqual(response.status_code, 200)

    def test_index_returns_no_articles(self):

        response = self.client.get(reverse('index'))
        self.assertContains(response, "No Articles")

    def test_index_returns_created_article(self):
        article = create_article(title="New Title", text="New Text")

        response = self.client.get(reverse('index'))
        self.assertContains(response, article.title)

class ArticleViewTests(TestCase):
    def test_article_view_returns_article(self):
        article = create_article(title="New Title", text="New Text")
        response = self.client.get(reverse("article", args=(article.id,)))
        self.assertContains(response, article.title)
        self.assertContains(response, article.text)
        self.assertContains(response, 'View Count: 1')

    def test_article_view_updates_view_count(self):
        article = create_article(title="New Title", text="New Text")
        self.client.get(reverse("article", args=(article.id,)))
        self.client.get(reverse("article", args=(article.id,)))
        response = self.client.get(reverse("article", args=(article.id,)))
        self.assertContains(response, 'View Count: 3')