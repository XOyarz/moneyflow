from django.urls import path

from . import views

urlpatterns = [
    path('', views.IndexView.as_view(), name='index'),
    path('article/<int:article_id>/', views.view_article, name='article'),
    path('article/create', views.create_article, name='create_article'),
    path('article/<int:article_id>/edit', views.edit_article, name='edit_article'),
    path('article/search', views.search_article, name="search"),
]