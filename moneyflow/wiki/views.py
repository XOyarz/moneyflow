from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect
from wiki.models import Article
from django.db.models import Q
from django.template import loader
from django.urls import reverse
from django.views import generic
from .forms import ArticleForm, ArticleCreateForm, ArticleEditForm
from django.utils import timezone

# Create your views here.
class IndexView(generic.ListView):
    template_name = 'wiki/index.html'
    context_object_name = 'articles'

    def get_queryset(self):
        return Article.objects.order_by('-created')[:10]

def view_article(request, article_id):
    article = get_object_or_404(Article, pk=article_id)
    article.view_count += 1
    article.save()
    if article.related_articles:
        related_articles = [article for article in article.related_articles.all()]
    return render(request, 'wiki/article.html', {'article':article, 'related_articles': related_articles})

def edit_article(request, article_id):
    article = get_object_or_404(Article, pk=article_id)
    if request.method == "POST":
        form = ArticleEditForm(request.POST, instance=article)
        form.validate_related_articles(article_id=article_id)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse("article", args=(article_id,)))
    else:
        form = ArticleEditForm(instance=article)
    return render(request, 'wiki/edit_article.html', {'form': form})

def create_article(request):
    if request.method == "POST":
        form = ArticleCreateForm(request.POST)
        if form.is_valid():
            article = form.save(commit=False)
            article.created=timezone.now()
            article.save()
            return HttpResponseRedirect(reverse("article", args=(article.id,)))
    else:
        form = ArticleCreateForm()
    return render(request, 'wiki/create_article.html', {'form':form})

def search_article(request):
    if request.method == "POST":
        search_term = request.POST['to_search']
        articles = Article.objects.filter(            
            Q(title__icontains=search_term) | Q(text__icontains=search_term)
        )
        return render(request, "wiki/search.html", {"found_articles": articles, "search_term": search_term})
    else:
        return render(request, "wiki/search.html", {})